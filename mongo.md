## Zadanie 1.
```js
db.pracownicy.find({})
```
Pracowników jest 19
Data przedstawiona jest w formacie ISODate

## Zadanie 2.
```js
db.zespoly.insert([
{"id_zesp":10,"nazwa":"ADMINISTRACJA","adres":"PIOTROWO 3A"},
{"id_zesp":20,"nazwa":"SYSTEMY ROZPROSZONE","adres":"PIOTROWO 3A"},
{"id_zesp":30,"nazwa":"SYSTEMY EKSPERCKIE","adres":"STRZELECKA 14"},
{"id_zesp":40,"nazwa":"ALGORYTMY","adres":"WLODKOWICA 16"},
{"id_zesp":50,"nazwa":"BADANIA OPERACYJNE","adres":"MIELZYNSKIEGO 30"}
]);
```

## Zadanie 3

Po wykonaniu pierwszego polecenia otrzymujemy tylko nazwisko
Po wykonaniu drugiego polecenia uzyskujemy wszystkie pola poza nazwiskiem i _id
 
## Zadanie 4
 
```js
db.pracownicy.find(
    {$or: [
        {"etat": "ASYSTENT"},
        {$and: [
            {"placa_pod": {$gte: 200}},
            {"placa_pod": {$lte: 500}},
        ]}
    ]},
    {
        "_id": 0,
        "nazwisko": 1,
        "etat": 1,
        "placa_pod": 1,
    }
)
```
 
## Zadanie 5

```js
db.pracownicy.find(
    {"placa_pod": {$gt: 400}},
    {
        "_id": 0,
        "nazwisko": 1,
        "etat": 1,
        "placa_pod": 1,
    }
).sort({
    "etat": 1,
    "placa_pod": -1,
})
```

## Zadanie 6
 
```js
db.pracownicy.find(
    {"id_zesp": 20},
    {
        "_id": 0,
        "nazwisko": 1,
        "placa_pod": 1,
    }
)
.sort({"placa_pod": -1})
.skip(1)
.limit(1)
```
 
## Zadanie 7
 
```js
db.pracownicy.find(
    {$and: [
        {"id_zesp": {$in: [20, 30]}},
        {"etat": {$ne: "ASYSTENT"}},
        {"nazwisko": {$regex: /I$/}},
    ]},
    {
        "_id": 0,
        "nazwisko": 1,
        "etat": 1,
    }
)
```
 
## Zadanie 8
 
```js
db.pracownicy.aggregate([
    {$project: {
       "_id": 0,
       "stanowisko": "$etat",
       "nazwisko": 1,
       "rok_zatrudnienia": {$year: "$zatrudniony"}
    }},
    {$sort: {"placa_pod": -1}},
    {$skip: 2},
    {$limit: 1},
])
```
 
## Zadanie 9
 
```js
db.pracownicy.aggregate([
    {$group: {
        "_id": "$id_zesp",
        "liczba": {$sum: 1},
    }},
    {$match: {
        "liczba": {$gt: 3},
    }},
])
```
 
## Zadanie 10
 
```js
db.pracownicy.aggregate([
    {$lookup: {
        from: "zespoly",
        localField: "id_zesp",
        foreignField: "id_zesp",
        as: "dept",
    }},
    {$match: {
        "id_zesp": {$in: [20, 30]},
    }},
    {$project: {
        "_id": 0,
        "nazwisko": 1,
        "dept": {$arrayElemAt: ["$dept.adres", 0]},
    }},
])
``` 
 
## Zadanie 11
 
```js
db.pracownicy.aggregate([
    {$lookup: {
        from: "zespoly",
        localField: "id_zesp",
        foreignField: "id_zesp",
        as: "dept",
    }},
    {$match: {
        "dept.adres": {$regex: /STRZELECKA/},
    }},
    {$project: {
        "_id": 0,
        "nazwisko": 1,
        "dept": {$arrayElemAt: ["$dept.nazwa", 0]},
    }},
])
``` 
## Zadanie 12
 
```js
var pracownicy = db.pracownicy.find();
while (pracownicy.hasNext()) {
 prac = pracownicy.next();
 zesp = db.zespoly.findOne({"id_zesp": prac.id_zesp});
 db.pracownicy.update(
    {"_id": prac._id},
    {$set: {"id_zesp": zesp._id}}
 );
}
```

## Zadanie 13

```js
db.produkty.find(
 {"oceny.osoba": {"$not": {"$in":["Ania", "Karol"]}}},
 {_id:0, nazwa:1}
)
```

## Zadanie 14

```js
db.produkty.aggregate([
 {$unwind : "$oceny" },
 {$group: {
     _id:"$_id",
     produkt: {$first: "$nazwa"},
     srednia_ocena: {$avg: "$oceny.ocena"}
 }},
 {$sort: {srednia_ocena: -1}},
 {$limit: 1},
 {$project: {_id: 0}}
 ]
)
```

## Zadanie 15

```js
db.produkty.update(
 {"nazwa":"Kosiarka spalinowa"},
 {$push: {oceny: {osoba: "Jacek", ocena: 4 }}}
)
```

## Zadanie 16

```js
db.produkty.update(
 {},
 { $pull: { 
     "oceny": { "ocena": { $lte: 4 }} 
 }},
 { multi: true }
)
```


