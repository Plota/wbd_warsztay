/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaApplication4;
 
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
 
/**
 *
 * @author student
 */
public class JavaApplication4 {
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
   
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", "inf132308");
        connectionProps.put("password", "inf132308");
        try {
            conn = DriverManager.getConnection("jdbc:oracle:thin:@//admlab2.cs.put.poznan.pl:1521/dblab02_students.cs.put.poznan.pl",
            connectionProps);
            conn.setAutoCommit(false);
            System.out.println("Połączono z bazą danych");
           
            Statement stmt;
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
           
            
            ResultSet rs=stmt.executeQuery("SELECT count(*),id_zesp FROM pracownicy GROUP by id_zesp order by id_zesp");
            int suma = 0;
            while (rs.next()) {
             int ile=rs.getInt(1);
             suma = suma + ile;
             String str = rs.getString("id_zesp");
             
             System.out.println(ile + " pracownikow w zespole" + str);  
            }  
            System.out.println("łącznie " + suma + " pracownikow");  
            rs.close();
            
            //--------------------------------------------------//
            ResultSet rs2=stmt.executeQuery("SELECT * FROM pracownicy where etat = 'ASYSTENT' order by placa_pod + coalesce(0,placa_dod) desc");
            
            while (rs2.next()) {
             rs2.last();
             String nazwisko = rs2.getString("NAZWISKO");
             System.out.println(nazwisko);
             rs2.relative(-2);
             nazwisko = rs2.getString("NAZWISKO");
             System.out.println(nazwisko);
             rs2.relative(-2);
             nazwisko = rs2.getString("NAZWISKO");
             System.out.println(nazwisko);
             rs2.last();
            }  
            
            rs2.close();
            
            
            
            //------------------------------------------------------------//
            String [] zwolnienia={"150", "200", "230"};
            String [] zatrudnienia={"Kandefer", "Rygiel", "Boczar"};
            int changes;
           
            for(int i=0;i<3;i++)
                {
                   String str = "DELETE FROM pracownicy WHERE id_prac=";
                   str = str + zwolnienia[i];
                   changes=stmt.executeUpdate(str);
                   System.out.println("Usunięto " + changes + " krotek");
                }
            
            for(int i=0;i<3;i++)
                {
                   String str = "INSERT INTO pracownicy(id_prac,nazwisko)"+
                    "VALUES("+zwolnienia[i] + ",'" + zatrudnienia[i] + "')";
                   changes=stmt.executeUpdate(str);
                   System.out.println("wsawiono " + changes + " krotek");
                }
            

           //-------------------------------------------------------------//
           ResultSet rs3=stmt.executeQuery("SELECT * FROM etaty");
           while (rs3.next()) {

             String str = rs3.getString("NAZWA");
             
             System.out.println(str);  
            }  
            rs3.close();

            conn.rollback();
            stmt.close();
            
            
        } catch (SQLException ex) {
            Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE,
            "nie udało się połączyć z bazą danych", ex);
            System.exit(-1);
        }
       
       
        try {
            System.out.println("Rozłączono z bazą danych");
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
   
}