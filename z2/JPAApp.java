/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaapp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Mateusz
 */
public class JPAApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("JPAAppPU");
        EntityManager em = factory.createEntityManager();
        
        //--------------------Ä†W 5.1-------------------------------------------------------//
        Zespol zesp = em.find(Zespol.class, (int)10);
        System.out.println(zesp.getIdZesp());
        System.out.println(zesp.getNazwa());
        System.out.println(zesp.getAdres());
        System.out.println(" ");
        
        //--------------------Ä†W 5.2-------------------------------------------------------//
        Query query=em.createQuery("SELECT z FROM Zespol z " + "ORDER BY z.nazwa");
        List<Zespol> listaZ = query.getResultList();
        for(Zespol z : listaZ)
            System.out.println(z.getIdZesp() + " " + z.getNazwa() + " " + z.getAdres());
        
        System.out.println(" ");
        //--------------------Ä†W 5.3-------------------------------------------------------//
        query=em.createNamedQuery("Zespol.findAll");
        List<Zespol> listaZ2 = query.getResultList();
        for(Zespol z : listaZ2)
            System.out.println(z.getIdZesp() + " " + z.getNazwa() + " " + z.getAdres());
        System.out.println(" ");
        //------------------6.1----------------//
        Zespol ze = new Zespol();
        ze.setIdZesp((int)70);
        ze.setNazwa("Zespol Testowy");
        ze.setAdres("Kutrzeby");
        em.getTransaction().begin();
        em.persist(ze);
        em.getTransaction().commit();
        
        query=em.createNamedQuery("Zespol.findAll");
        List<Zespol> listaZ61 = query.getResultList();
        for(Zespol z : listaZ61)
            System.out.println(z.getIdZesp() + " " + z.getNazwa() + " " + z.getAdres());
        System.out.println(" ");
        
        //------------------6.3----------------//
        Zespol ze2 = new Zespol();
        ze2.setNazwa("Test Sekwencji");
        em.getTransaction().begin();
        em.persist(ze2);
        em.getTransaction().commit();
        
        //------------------6.4----------------//
        Zespol zesp70 = em.find(Zespol.class, (int)70);
        Zespol zesp72 = em.find(Zespol.class, (int)72);
        em.getTransaction().begin();
        em.remove(zesp70);
        em.remove(zesp72);
        em.getTransaction().commit();
        
        //------------------7.3----------------//
        query=em.createNamedQuery("Pracownik.findAll");
        List<Pracownik> listaP = query.getResultList();
        for(Pracownik p : listaP)
            System.out.println(p.getIdPrac()+ " " + p.getNazwisko() + " " + p.getPlacaPod() + " " +p.getIdZesp().getNazwa());
        
        Pracownik prac = new Pracownik();
        prac.setNazwisko("Tomkowiak");
        prac.setIdPrac((int)400);
        prac.setPlacaPod((float) 1600);
        em.getTransaction().begin();
        em.persist(prac);
        em.getTransaction().commit();
        
        ArrayList<Pracownik> listaPracownikowDoPodwyzki = new ArrayList<>();
        listaP.forEach(p -> {
            if (prac.getPlacaPod() < 3000) {
                listaPracownikowDoPodwyzki.add(p);
            }
        });
        em.getTransaction().begin();
        listaPracownikowDoPodwyzki.forEach(p -> {
            prac.setPlacaPod(p.getPlacaPod() * (float) 1.1);
            em.persist(p);
        });
        em.getTransaction().commit();
        
        //------------------7.4----------------//
        query=em.createNamedQuery("Zespol.findAll");
        List<Zespol> listaZesp = query.getResultList();
        listaZesp.forEach(z -> {
            Collection<Pracownik> pracownicyZespolu = z.getPracownicyCollection();
            System.out.println(z.getNazwa());
            for(Pracownik p : pracownicyZespolu) {
                System.out.println(p.getIdPrac() + " " + p.getNazwisko());
            }
        });
    }
    
}
