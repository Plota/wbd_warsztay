/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaapp;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mateusz
 */
@Entity
@Table(name = "ZESPOLY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Zespol.findAll", query = "SELECT z FROM Zespol z"),
    @NamedQuery(name = "Zespol.findByIdZesp", query = "SELECT z FROM Zespol z WHERE z.idZesp = :idZesp"),
    @NamedQuery(name = "Zespol.findByNazwa", query = "SELECT z FROM Zespol z WHERE z.nazwa = :nazwa"),
    @NamedQuery(name = "Zespol.findByAdres", query = "SELECT z FROM Zespol z WHERE z.adres = :adres")})
public class Zespol implements Serializable {
    @OneToMany(mappedBy = "idZesp")
    private Collection<Pracownik> pracownicyCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="SEQ_JPA_ZESPOLY", allocationSize=5)
    @Column(name = "ID_ZESP")
    private Integer idZesp;
    @Column(name = "NAZWA")
    private String nazwa;
    @Column(name = "ADRES")
    private String adres;

    public Zespol() {
    }

    public Zespol(Integer idZesp) {
        this.idZesp = idZesp;
    }

    public Integer getIdZesp() {
        return idZesp;
    }

    public void setIdZesp(Integer idZesp) {
        this.idZesp = idZesp;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idZesp != null ? idZesp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zespol)) {
            return false;
        }
        Zespol other = (Zespol) object;
        if ((this.idZesp == null && other.idZesp != null) || (this.idZesp != null && !this.idZesp.equals(other.idZesp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpaapp.Zespoly[ idZesp=" + idZesp + " ]";
    }

    @XmlTransient
    public Collection<Pracownik> getPracownicyCollection() {
        return pracownicyCollection;
    }

    public void setPracownicyCollection(Collection<Pracownik> pracownicyCollection) {
        this.pracownicyCollection = pracownicyCollection;
    }
    
}
