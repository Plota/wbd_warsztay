/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaapp;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author student
 */
public class JPAApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("JPAAppPU");
        EntityManager em = emf.createEntityManager();
        
        //--------------------ĆW 5.1-------------------------------------------------------//
        Zespol zesp = em.find(Zespol.class, (short)10);
        System.out.println(zesp.getIdZesp());
        System.out.println(zesp.getNazwa());
        System.out.println(zesp.getAdres());
        System.out.println(" ");
        
        //--------------------ĆW 5.2-------------------------------------------------------//
        Query query=em.createQuery("SELECT z FROM Zespol z " + "ORDER BY z.nazwa");
        List<Zespol> listaZ = query.getResultList();
        for(Zespol z : listaZ)
            System.out.println(z.getIdZesp() + " " + z.getNazwa() + " " + z.getAdres());
        
        System.out.println(" ");
        //--------------------ĆW 5.3-------------------------------------------------------//
        query=em.createNamedQuery("Zespol.findAll");
        List<Zespol> listaZ2 = query.getResultList();
        for(Zespol z : listaZ2)
            System.out.println(z.getIdZesp() + " " + z.getNazwa() + " " + z.getAdres());
        System.out.println(" ");
        
        
        //--------------------ĆW 6-------------------------------------------------------//
        /*Zespol ze = new Zespol();
        ze.setIdZesp((short)70);
        ze.setNazwa("Zespol Tesowy");
        ze.setAdres("Kutrzeby");
        em.getTransaction().begin();
        em.persist(ze);
        em.getTransaction().commit();
        */
        
        

    }
    
}
