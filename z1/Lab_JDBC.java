/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateusz
 */
public class Lab_JDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", "inf132333");
        connectionProps.put("password", "inf132333");
        try {
            conn = DriverManager.getConnection("jdbc:oracle:thin:@//admlab2.cs.put.poznan.pl:1521/dblab02_students.cs.put.poznan.pl",
            connectionProps);
            conn.setAutoCommit(false);
            System.out.println("PoĹ‚Ä…czono z bazÄ… danych");
           
            Statement stmt;
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
           
            
            ResultSet rs=stmt.executeQuery("SELECT count(*),id_zesp FROM pracownicy GROUP by id_zesp order by id_zesp");
            int suma = 0;
            while (rs.next()) {
             int ile=rs.getInt(1);
             suma = suma + ile;
             String str = rs.getString("id_zesp");
             
             System.out.println(ile + " pracownikow w zespole" + str);  
            }  
            System.out.println("Ĺ‚Ä…cznie " + suma + " pracownikow");  
            rs.close();
            
            //--------------------------------------------------//
            ResultSet rs2=stmt.executeQuery("SELECT * FROM pracownicy WHERE etat = 'ASYSTENT' ORDER BY placa_pod + coalesce(0,placa_dod) desc");
            
            while (rs2.next()) {
             rs2.last();
             String nazwisko = rs2.getString("NAZWISKO");
             System.out.println(nazwisko);
             rs2.relative(-2);
             nazwisko = rs2.getString("NAZWISKO");
             System.out.println(nazwisko);
             rs2.relative(-2);
             nazwisko = rs2.getString("NAZWISKO");
             System.out.println(nazwisko);
             rs2.last();
            }  
            
            rs2.close();
            
            
            
            //------------------------------------------------------------//
            String [] zwolnienia={"150", "200", "230"};
            String [] zatrudnienia={"Kandefer", "Rygiel", "Boczar"};
            int changes;
           
            for(int i=0;i<3;i++)
                {
                   String str = "DELETE FROM pracownicy WHERE id_prac=";
                   str = str + zwolnienia[i];
                   changes=stmt.executeUpdate(str);
                   System.out.println("UsuniÄ™to " + changes + " krotek");
                }
            
            for(int i=0;i<3;i++)
                {
                   String str = "INSERT INTO pracownicy(id_prac,nazwisko)"+
                    "VALUES("+zwolnienia[i] + ",'" + zatrudnienia[i] + "')";
                   changes=stmt.executeUpdate(str);
                   System.out.println("wsawiono " + changes + " krotek");
                }
            

           //-------------------------------------------------------------//
           conn.setAutoCommit(false);
           stmt = conn.createStatement();
           ResultSet rs3 = stmt.executeQuery("SELECT * FROM etaty");
           while (rs3.next()) {

             String str = rs3.getString("NAZWA");
             
             System.out.println(str);  
            }  
            rs3.close();

            ResultSet rs4 = stmt.executeQuery("INSERT INTO ETATY(nazwa, placa_min, placa_max)"+
                    " VALUES (NOWY_ETAT, 400, 800)");
            rs4.close();
            
            ResultSet rs5 = stmt.executeQuery("SELECT * FROM etaty");
            while (rs5.next()) {

              String str = rs5.getString("NAZWA");

              System.out.println(str);  
             }  
            rs5.close();
            
            conn.rollback();
            
             ResultSet rs6 = stmt.executeQuery("SELECT * FROM etaty");
            while (rs6.next()) {

              String str = rs6.getString("NAZWA");

              System.out.println(str);  
             }  
             rs6.close();

            ResultSet rs7 = stmt.executeQuery("INSERT INTO ETATY(nazwa, placa_min, placa_max)"+
                    " VALUES (NOWY_ETAT, 400, 800)");
            conn.commit();
            rs7.close();
            ResultSet rs8 = stmt.executeQuery("SELECT * FROM etaty");
            while (rs8.next()) {

              String str = rs8.getString("NAZWA");

              System.out.println(str);  
             }
            rs8.close();
            stmt.close();
            //-------------------------------------------------------------//
            String [] nazwiska={"Woźniak", "Dąbrowski", "Kozłowski"};
            int [] place={1300, 1700, 1500};
            String []etaty={"ASYSTENT", "PROFESOR", "ADIUNKT"};

            PreparedStatement pstmt = conn.prepareStatement(
                    "INSERT INTO PRACOWNICY(nazwisko, placa_pod, etat) VALUES (?, ?, ?)");
            for(int i = 0; i < 3; i++) {
                pstmt.setString(1, nazwiska[i]);
                pstmt.setFloat(2, place[i]);
                pstmt.setString(3, etaty[i]);

                ResultSet rsZ5 = pstmt.executeQuery();
                rsZ5.close();
            }

            //-------------------------------------------------------------//
            
            PreparedStatement pstmt2 = conn.prepareStatement(
                    "INSERT INTO PRACOWNICY(nazwisko, placa_pod, etat) VALUES (?, ?, ?)");
            
            long start = System.nanoTime(); 
            for(int i = 0; i < 2000; i++) {
                pstmt2.setString(1, "Nowy prac");
                pstmt2.setFloat(2, new Float(1234.2));
                pstmt2.setString(3, "ADIUNKT");
                
                ResultSet rsZ6 = pstmt2.executeQuery();
                rsZ6.close();
            }
            long time = System.nanoTime() - start; 
            System.out.println(time);
            
            long start2 = System.nanoTime(); 
            for(int i = 0; i < 2000; i++) {
                pstmt2.setString(1, "Nowy prac");
                pstmt2.setFloat(2, new Float(1234.2));
                pstmt2.setString(3, "ADIUNKT");
                pstmt2.addBatch();

                
            }
            int[] rsZ62 = pstmt2.executeBatch();
            long time2 = System.nanoTime() - start2; 
            System.out.println(time2);
            
            //-------------------------------------------------------------//
            
            CallableStatement cstmt = conn.prepareCall("{? = call ZmienNazwisko(?, ?)}");
            cstmt.setInt(2, 220);
            cstmt.registerOutParameter(3, Types.VARCHAR);
            cstmt.registerOutParameter(1, Types.NUMERIC);
            cstmt.execute();
            int hasChange = cstmt.getInt(1);
            cstmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(Lab_JDBC.class.getName()).log(Level.SEVERE,
            "nie udaĹ‚o siÄ™ poĹ‚Ä…czyÄ‡ z bazÄ… danych", ex);
            System.exit(-1);
        }
       
       
        try {
            System.out.println("RozĹ‚Ä…czono z bazÄ… danych");
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Lab_JDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
